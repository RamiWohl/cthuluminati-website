<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Models\Gig;

class GigsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allGigs = Gig::all();
        $allGigs = $allGigs->sortByDesc(function ($gig) {
            return $gig->date;
        })->values();

        return json_encode($allGigs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = json_decode($request->getContent(), true);
        $rules = [
            'title' => 'required',
            'date' => 'required|date',
            'venue' => 'required',
            'city' => 'required',
            'country' => 'required',
            'cancelled' => 'required|boolean'
        ];

        $validator = Validator::make($data['show'], $rules);

        if (!$validator->passes()) {
            return response()->json($validator->errors(), 422);
        }

        $newGig = Gig::create($data['show']);

        return json_encode($newGig);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = json_decode($request->getContent(), true);
        $rules = [
            'title' => 'required',
            'date' => 'required|date',
            'venue' => 'required',
            'city' => 'required',
            'country' => 'required',
            'cancelled' => 'required|boolean'
        ];

        $validator = Validator::make($data['show'], $rules);

        if (!$validator->passes()) {
            return response()->json($validator->errors(), 422);
        }

        $gig = Gig::find($id);

        if (!$gig) {
            return response()->json(['message' => 'cannot find that one'], 422);
        }

        $gig->update($data['show']);
        $gig->fresh();

        return json_encode($gig);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gig = Gig::find($id);

        if (!$gig) {
            return response()->json(['message' => 'cannot find that one'], 422);
        }

        $gig->delete();

        return json_encode(['id' => $id]);
    }
}
