import React from "react";
import { routes } from "./../routes";
import { Link } from "react-router-dom";
import styled from "styled-components";

const HeaderStyled = styled.nav`
    height: 35px;
    position: relative;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    padding: 10px 1rem 10px;
    background-color: #0a0a0a;
    backface-visibility: hidden;
`;

const StyledLink = styled(Link)`
    display: inline-block;
    padding-top: 0.32rem;
    padding-bottom: 0.32rem;
    margin: 0 25px;
    line-height: inherit;
    white-space: nowrap;
    font-family: Century Gothic, CenturyGothic, AppleGothic, sans-serif;
    font-size: 1.1rem;
    font-weight: 400;
    line-height: 1.6;
    text-decoration: none;
    color: #fff9;
`;

export const StyledBurger = styled.button`
    position: absolute;
    top: 22%;
    left: 1rem;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    transform: scale(0.8);
    width: 2rem;
    height: 2rem;
    background: transparent;
    border: none;
    cursor: pointer;
    padding: 0;
    z-index: 10;

    &:focus {
        outline: none;
    }

    div {
        width: 2rem;
        height: 0.15rem;
        border-radius: 10px;
        transition: all 0.3s linear;
        background-color: white;
        position: relative;
        transform-origin: 1px;
    }

    @media (min-width: 500px) {
        display: none;
    }
`;

const NavContainer = styled.div`
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;

    @media (max-width: 500px) {
        display: none;
    }
`;

const Burger = ({ showMobileMenu, setShowMobileMenu }) => {
    return (
        <StyledBurger
            showMobileMenu={showMobileMenu}
            onClick={() => setShowMobileMenu(!showMobileMenu)}
        >
            <div />
            <div />
            <div />
        </StyledBurger>
    );
};

const Header = ({ showMobileMenu, setShowMobileMenu }) => {
    return (
        <HeaderStyled>
            <Burger
                showMobileMenu={showMobileMenu}
                setShowMobileMenu={setShowMobileMenu}
            />
            <NavContainer>
                <StyledLink to={routes.main}>Main</StyledLink>
                <StyledLink to={routes.bio}>Bio</StyledLink>
                <StyledLink to={routes.shows}>Shows</StyledLink>
                <StyledLink to={routes.stories}>Stories</StyledLink>
                <StyledLink to={{ pathname: routes.store }} target="_blank">
                    Store
                </StyledLink>
            </NavContainer>
        </HeaderStyled>
    );
};

export default Header;
