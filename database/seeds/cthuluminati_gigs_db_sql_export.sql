insert into
  gigs(
    title,
    venue,
    city,
    country,
    date,
    cancelled,
    created_at,
    updated_at
  )
values(
    'Heiland Festival',
    'Xinix',
    'Nieuwendijk',
    'NL',
    '2019-05-18',
    '0',
    null,
    null
  ),(
    'With Jahmean',
    'Cab03',
    'Leiden',
    'NL',
    '2019-05-31',
    '0',
    null,
    null
  ),(
    'Solo performance',
    'Little Devil',
    'Tilburg',
    'NL',
    '2019-09-16',
    '0',
    null,
    null
  ),(
    'With Jahmean',
    'Lola',
    'Groningen',
    'NL',
    '2019-08-02',
    '0',
    null,
    null
  ),(
    'Herfstfestival',
    'Xinix',
    'Nieuwendijk',
    'NL',
    '2019-11-16',
    '0',
    null,
    null
  ),(
    'With Façade and Beyond our Ruins',
    'Popcentrale',
    'Dordrecht',
    'NL',
    '2019-11-29',
    '0',
    null,
    null
  ),(
    'Rotterdam Metal',
    'Bar3',
    'Rotterdam',
    'NL',
    '2019-12-08',
    '0',
    null,
    null
  ),(
    'With Adversarius and Hellewijt',
    'Podium Gorcum',
    'Gorinchem',
    'NL',
    '2020-02-08',
    '0',
    null,
    null
  ),(
    'With Laster, Përl and Sarcofvck',
    'Musikbunker',
    'Aachen',
    'DE',
    '2020-02-10',
    '0',
    null,
    null
  ),(
    'With Dark Fortress and Morvigor',
    'Baroeg',
    'Rotterdam',
    'NL',
    '2020-03-29',
    '1',
    null,
    null
  ),(
    'Achterhoeks Metalfestival',
    'Openluchttheater',
    'Eibergen',
    'NL',
    '2020-05-16',
    '1',
    null,
    null
  ),(
    'With Jahmean and Release the River',
    'De Vrijplaats',
    'Leiden',
    'NL',
    '2020-09-12',
    '1',
    null,
    null
  ),(
    'Festerfest',
    'Musicon',
    'Den Haag',
    'NL',
    '2020-10-03',
    '1',
    null,
    null
  ),(
    'Festival',
    'Grey Area',
    'Tilburg',
    'NL',
    '2022-05-27',
    '0',
    null,
    null
  );