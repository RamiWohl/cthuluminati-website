import styled from "styled-components";

const MainPageLayout = styled.div`
    display: flex;
    justify-content: center;
    height: fit-content;
`;

export default MainPageLayout;
