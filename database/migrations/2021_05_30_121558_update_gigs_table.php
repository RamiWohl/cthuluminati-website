<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gigs', function (Blueprint $table) {
            $table->after('id', function ($table) {
                $table->string('title')->nullable();
                $table->string('venue');
                $table->string('city');
                $table->string('country');
                $table->date('date');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gigs', function (Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('venue');
            $table->dropColumn('city');
            $table->dropColumn('country');
            $table->dropColumn('date');
        });
    }
}
