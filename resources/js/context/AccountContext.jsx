import React, { useReducer, createContext } from "react";
import { useHistory } from "react-router-dom";
import { accountReducer, initialState } from "./reducers/accountReducer";
import httpClient from "../auth/httpClient";
import { protectedRoutes } from "../routes";

export const AccountContext = createContext({
    state: initialState,
    dispatch: () => null,
});

export const AccountProvider = ({ children }) => {
    const [state, dispatch] = useReducer(accountReducer, initialState);
    const history = useHistory();

    const login = async (loginCredentials) => {
        dispatch({ type: "SET_LOADING" });
        try {
            await httpClient.get('/sanctum/csrf-cookie');

            const result = await httpClient.post(
                "/api/login",
                loginCredentials,
            );

            localStorage.setItem("userName", result.data.name);
            localStorage.setItem("userEmail", result.data.email);
            localStorage.setItem("userLoggedIn", true);

            dispatch({ type: "LOGIN", payload: result.data });

            history.replace({ pathname: protectedRoutes.adminDashboard });
        } catch (error) {
            dispatch({ type: "LOGIN_ERROR", payload: error });
        }
    };

    const checkAdminPermission = async () => {
        try {
            await httpClient.get("/api/check");
        } catch (error) {}
    };

    return (
        <AccountContext.Provider
            value={{
                account: state.account,
                loading: state.loading,
                error: state.error,
                login,
                checkAdminPermission,
            }}
        >
            {children}
        </AccountContext.Provider>
    );
};
