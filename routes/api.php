<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GigsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\StoriesController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    'users' => UsersController::class,
    'gigs' => GigsController::class,
    'stories' => StoriesController::class,
]);

Route::post('login', [LoginController::class, 'authenticate']);
Route::get('check', [LoginController::class, 'check']);

Route::fallback(function () {
    return response()->json(['message' => 'Not Found.'], 404);
});
